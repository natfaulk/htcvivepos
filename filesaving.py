import os.path

BASE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)))
SAVE_DIR = "data"
SAVE_PATH = os.path.join(BASE_PATH, SAVE_DIR)

currentIndex = 0

def makeOutputPath(prefix):
  return os.path.join(SAVE_PATH, f'{prefix}{currentIndex}')

def nextOutputDir(prefix):
  global currentIndex
  while os.path.isdir(makeOutputPath(prefix)):
    currentIndex += 1
  return makeOutputPath(prefix)

def mkdirp(_path):
  if not os.path.isdir(_path):
    os.makedirs(_path)

def setupDirs(trackerNames):
  mkdirp(SAVE_DIR)

  for i in trackerNames:
    f = os.path.join(SAVE_DIR, i)
    mkdirp(f)
