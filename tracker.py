import datetime
import json
import os
import time

import version

# pass openvr instance
def getTrackerNames(vr):
  return [i for i in vr.devices.keys() if i.startswith('tracker_')]


class Tracker:
  def __init__(self, vr, name, outDir):
    self.f = open(os.path.join(outDir, f'{name}.txt'), 'w')
    self.vr = vr
    self.name = name
    self.serial = self.vr.devices[self.name].get_serial()
    self.writeHeader()

    print(self.name, self.serial)

  def writeHeader(self):
    header = {
      'version':version.VERSION,
      'date': datetime.datetime.now().astimezone().isoformat(),
      'name': self.name,
      'serial': self.serial
    }
    self.f.write(f'Header|{json.dumps(header)}\n')

  def tick(self):
    data = self.vr.devices[self.name].get_pose_euler()
    print(self.name, data[0], data[1], data[2], time.time())
    
    out = {
      'pos': data,
      'time': time.time()
    }

    self.f.write(f'Data|{json.dumps(out)}\n')
