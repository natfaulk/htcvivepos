import time

import pygame
import triad_openvr

import filesaving
import tracker

PREFIX='positions'

if __name__ == '__main__':
  vr = triad_openvr.triad_openvr()
  trackerNames = tracker.getTrackerNames(vr)

  outdir = filesaving.nextOutputDir(PREFIX)
  print(f'Creating directory: {outdir}')
  filesaving.mkdirp(outdir)

  trackers = []
  for name in trackerNames:
    trackers.append(tracker.Tracker(vr, name, outdir))

  # sleep so we have time to see the information that tracker prints
  # in its init function
  time.sleep(5)

  clock = pygame.time.Clock()
  while True:
    clock.tick(90)
    for t in trackers:
      t.tick()
